#! /bin/bash

declare Username=vcn
#define the username when you install the operating system
declare testprint=yes
#if you need to test printer after setting up

#-------------------- beginning-------------------


#add Username to sudoers

sudo sh -c "echo \"$Username ALL=(ALL) NOPASSWD:ALL\" >> /etc/sudoers"

#printer setting up
#create a directory to download the packages
  mkdir -p brother-drivers/mfc-9970cdw
  cd brother-drivers/mfc-9970cdw

#download the packages for mfc9970cdw

  sudo wget -T 10 -nd -nc http://www.brother.com/pub/bsc/linux/packages/mfc9700lpr-1.1.2-1.i386.deb
  sudo wget -T 10 -nd -nc http://www.brother.com/pub/bsc/linux/packages/cupswrapperMFC9700-1.0.2-1.i386.deb

#download the scan packages
# wget -T 10 -nd -nc http://www.brother.com/pub/bsc/linux/packages/brscan-0.2.4-0.amd64.dsudo -H gedit /var/lib/polkit-1/localauthority/10-vendor.d/com.ubuntu.desktop.pklaeb
#   wget -T 10 -nd -nc http://www.brother.com/pub/bsc/linux/packages/brscan-skey-0.2.4-1.amd64.deb	

#install the printer driver
   sudo dpkg  -x cupswrapperMFC9700-1.0.2-1.i386.deb ./
   sudo dpkg  -x mfc9700lpr-1.1.2-1.i386.deb ./

#restart cups service
   sudo systemctl restart cups.service

#configure printer parameters and ip address
   sudo /usr/sbin/lpadmin -p mfc9970cdw -E -P /usr/share/ppd/brmfc9970cdw.ppd
   sudo /usr/sbin/lpadmin -p mfc9970cdw -v socket://192.168.20.51 -E -P /usr/share/ppd/brmfc9970cdw.ppd


#print test page
  if [ $testprint == 'yes' ] 
   then
     lpr -P mfc9970cdw /usr/share/cups/data/testprint
   fi

#guest session setting up
 sudo sed -i s/'SystemAccount=true'/'SystemAccount=false'/ /var/lib/AccountsService/users/$Username

#setup turn off the screen never
 sudo sed -i '/# disable screen locking (MATE)/ i gsettings set org.gnome.desktop.session idle-delay 0' /usr/lib/lightdm/guest-session-auto.sh






